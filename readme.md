## Tokens
JGKV5P7bzzqjvTLzRWzt
## Testing Config
<pre>
stages:
  - test
  - deploy

test:
  stage: test
  script: sh test

deploy to production:
  stage: deploy
  script: make deploy
  environment:
    name: production
    url: https://example.com/
</pre>
